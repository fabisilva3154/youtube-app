<?php

use Doctrine\Common\Collections\ArrayCollection;

class Playlist
{
    protected $id;
    protected $name;
    protected $videos;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getVideos()
    {
        return $this->videos->toArray();
    }

    public function setVideos($videos)
    {
        $this->videos = $videos;
    }

    public function addVideo(VideoInPlaylist $video)
    {
        $this->videos->add($video);
        $video->setPlaylist($this);
        return $this;
    }

    public function removeVideo(VideoInPlaylist $video)
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            $video->setPlaylist(null);
        }
        $i = 1;
        foreach ( $this->getVideos() as $key => $v ){
            $v->setNumOrder($i);
            $i++;
        }
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
}
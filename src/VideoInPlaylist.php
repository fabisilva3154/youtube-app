<?php
class VideoInPlaylist
{
    protected $id;
    protected $video;
    protected $playlist;
    protected $numOrder;

    public function getId()
    {
        return $this->id;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setVideo(Video $video = null)
    {
        $this->video = $video;
        return $this;
    }

    public function getPlaylist()
    {
        return $this->playlist;
    }

    public function setPlaylist(Playlist $playlist = null)
    {
        $this->playlist = $playlist;
        return $this;
    }

    public function getNumOrder()
    {
        return $this->numOrder;
    }

    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;
    }
    
}
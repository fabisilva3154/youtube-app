  app.models.Video = Backbone.Model.extend({
    
    defaults : {
      video_id     : '',
      played       : false,
      is_playing   : false,
    },

    initialize : function(){
      //this.get_info();
    },

    get_video_id : function(){
      return this.get('video_id');
    },

    get_title : function(){
      if ( this.get('title') ) return this.get('title');
      return this.get('video_info') ? this.get('video_info').items[0].snippet.title : 'Cargando '+this.get_video_id();
    },

    get_thumbnail : function(){
      if ( this.get('thumbnail') ) return this.get('thumbnail');
      return this.get('video_info') ? this.get('video_info').items[0].snippet.thumbnails.default.url : '';
    },

    get_num_order : function(playlist){
      var $this = this;
      var i = 1;
      var order = 1;
      playlist.each(function(video){
        if ( video.cid == $this.cid ){
          order = i;
        }
        i++;
      });
      return order;
    },

    set_video_id_by_url : function(url){
      if (url.indexOf('feature') != -1){
          start_slice = url.indexOf('&v=')+3;
          end_slice = url.substr(url.indexOf('&')+1).indexOf('&') != -1 ? url.indexOf('&') : url.length;
      } else {
          start_slice = url.indexOf('?v=')+3;
          end_slice = url.indexOf('&') != -1 ? url.indexOf('&') : url.length;
      }
      video_id  = url.slice(start_slice, end_slice);
      this.set('video_id', video_id);
      return video_id;
    },

    is_valid : function(){
      return this.get('video_id').length <= 14 && this.get('video_id').length >= 8;
    },

    is_saved : function(){
      return this.get('saved');
    },

    set_played : function(value){
      this.set('played', value);
    },

    played : function(){
      return this.get('played');
    },

    is_playing : function(){
      return this.get('is_playing');
    },

    set_playing : function(value){
      return this.set('is_playing', value);
    },

    get_info   : function(callback){
      var $this = this;
      if ( this.get('video_info') ) { 
        callback(this.get('video_info')); 
        return ;
      }
      $.ajax({
        url      : 'https://www.googleapis.com/youtube/v3/videos?id='+this.get_video_id()+'&key=AIzaSyAldqwLI-PJ1uLXowjR7rvnCzQSii_9RLc&part=snippet,contentDetails,statistics,status',
        dataType :'json',
        success  : function(video_info){
          $this.set('video_info', video_info);
          if (callback) { callback(video_info);}
        },
        error   : function(e){
          if (callback) { callback(e);}
        }
      });
    },

  });

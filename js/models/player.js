
//requires app.collections.Videos

app.models.Player = Backbone.Model.extend({

  defaults      : {
    loaded      : false,
    first_video : true,
  },

  initialize    : function(){
    this.set('playlist', new app.collections.Videos());
  },

  is_first_video : function(){
    return this.get('first_video');
  },

  set_first_video : function(value){
    this.trigger('first-video', value);
    return this.set('first_video', value);
  },

  set_player    : function(player){
    return this.set('player', player);
  },

  get_player    : function(){
    return this.get('player');
  },

  ended    : function(){
    return typeof(YT) !== 'undefined' ? this.get_player().getPlayerState() == YT.PlayerState.ENDED : this.get_player().getPlayerState() == 0;
  },

  add_video   : function(video){
    this.get_playlist().add(video);
    this.trigger('video-added', video);
  },

  remove_video   : function(video){
    this.get_playlist().remove(video);
    this.trigger('video-removed', video);
  },

  play_video    : function(video){
    this.get_player().loadVideoById(video.get_video_id());
    var video_founded = false;
    this.get_playlist().each(function(v){
      if (video.cid == v.cid){
        video_founded = true;
      } else if ( video_founded ){
        v.set_played(false);
        v.set_playing(false);
      } else {
        v.set_playing(false);
        v.set_played(true);
      }
    });
    video.set_played(true);
    video.set_playing(true);
  },

  play_next     : function(){
    if ( this.get_current() ){
      this.get_current().set_playing(false);
    }
    this.play_video(this.get_next());
  },

  get_next      : function(){
    return this.get_playlist().get_next_video_to_play();
  },

  get_current : function(){
    return this.get_playlist().get_current_video();
  },

  get_video_by_id : function(video_id){
    return this.get_playlist().get_video_by_id(video_id);
  },

  get_video_by_cid : function(cid){
    return this.get_playlist().get(cid);
  },

  get_playlist  : function(){
    return this.get('playlist');
  },

  set_playlist  : function(playlist){
    return this.set('playlist', playlist);
  },

  fetch_playlist :function(playlist_id){
    var $this = this;
    $.ajax({
      url      : 'php_ajax/get_videos_by_playlist.php',
      data     : { id : playlist_id },
      dataType : 'json',
      success  : function(videos){
        if (videos.length){
          _.each(videos, function(video){
            var video = new app.models.Video(video);
            $this.add_video(video);
          });
          if ( !app.api_loaded ){
            app.add_api_loaded_callback(function(){
              $this.set_first_video(false);
              $this.load_player();
            });
          } else {
             $this.set_first_video(false);
             $this.load_player();
          }
        }
      },
    });
  },

  load_player   : function(){
    if ( !app.api_loaded ) { return false; }
    var video = this.get_next();
    if (typeof(YT) !== 'undefined') {
      this.set_player(new YT.Player('ytplayer', {
        height     : '390',
        width      : '640',
        videoId    : video.get_video_id(),
        playerVars : { 'autoplay': 1, 'iv_load_policy' : 3 },
        events     : {
          'onStateChange': check_status
        }
      }));
    } 
    video.set_played(true);
    video.set_playing(true);
    this.trigger('player-loaded');
  },

});

app.models.TestPlayer = Backbone.Model.extend({
  defaults : { state : 0 },
  getPlayerState : function(){
    return this.get('state');
  },

  loadVideoById : function(video_id){
    console.log('loaded:', video_id);
  },
  setPlayerState : function(state){
    this.set('state',state);
    "magmkagsmkasgm"
  }
});

app.player = new app.models.Player();
app.player.on('video-added', function(video){
  if ( video.is_saved() ) return ;
  $.ajax({
    url  : 'php_ajax/add_video.php',
    type : 'POST', 
    data : { 
      id        : app.id, 
      video_id  : video.get_video_id(), 
      thumbnail : video.get_thumbnail(), 
      title     : video.get_title(), 
      numOrder  : video.get_num_order(app.player.get_playlist()) 
    },
  });
});

app.player.on('video-removed', function(video){
  $.ajax({
    url  : 'php_ajax/remove_video.php',
    type : 'POST', 
    data : { 
      id        : app.id, 
      video_id  : video.get_video_id(), 
      numOrder  : video.get_num_order(app.player.get_playlist()) 
    },
  });
});

/* TEST*/
   // app.player.set_player(new app.models.TestPlayer());
   // app.api_loaded = true;
/*END TEST */

app.save_name_playlist = function(name){
  $.ajax({
    url  : 'php_ajax/save_name_playlist.php',
    data : { name : name },
    success : function(){
      app.name = name;
    }
  });  
};

$(document).ready(function(){

  $('#add-video').click(function(e){
    e.preventDefault();
    var video_url = $.trim($('#video-url').val());
    $('#video-url').val('');
    video = new app.models.Video();
    video.set_video_id_by_url(video_url);
    if ( !video.is_valid() ){
      alert('Pusiste cualquier cosa');
      return false;
    }
    video.get_info(function(){
        next_video = app.player.get_next();
        app.player.add_video(video);
        if ( app.player.is_first_video() ){
          app.player.set_first_video(false);
          app.player.load_player();
        } else if ( app.player.ended() && !next_video ) {
          app.player.play_video(video);
        }
    });
  });

  $('#sacame-el-video').click(function(e){
    app.player.play_next();
  });

  $('#table-container').on('click', '.eliminar', function(e){
    e.stopImmediatePropagation();
    var cid = $(e.currentTarget).data('cid');
    video = app.player.get_video_by_cid(cid);
    app.player.remove_video(video);
    if ( video.is_playing() && app.player.get_next() ){
      app.player.play_next()
    }
  });

  $('#table-container').on('click', '.video-tr', function(e){
    var cid = $(e.currentTarget).data('cid');
    video = app.player.get_video_by_cid(cid);
    app.player.play_video(video);
  });

  $('#show-playlists').click(function(){
    var playlists = new app.collections.Playlists();
    playlists.fetch({success : function(){
      template = _.template($('#playlist-list-template').html());
      $('#div-playlists').html(template({playlists : playlists}));
    }});
  });

  $('#table-container').on('click', '#title-playlist', function(e){
    var title = $(e.currentTarget);
    title.hide();
    var input = $('#table-container').find('#input-title-playlist');
    input.val(title.text());    
    input.show();
    input.focus();
  });

  //En el enter o en escape el input sale de foco
  $('#table-container').on('keydown', '#input-title-playlist', function(e){
    var input = $('#table-container').find('#input-title-playlist');
    var title = $('#table-container').find('#title-playlist');
    if ( e.keyCode == 27 || e.keyCode == 13 ){
      input.blur();
    }
  });

  //Cuando el input sale de foco se guarda el nombre de la playlists
  $('#table-container').on('focusout', '#input-title-playlist', function(e){
    var input = $('#table-container').find('#input-title-playlist');
    var title = $('#table-container').find('#title-playlist');
    input.hide();
    title.text(input.val());
    title.show();
    app.save_name_playlist(input.val());
  });

});

app.player.get_playlist().on('change',reload_playlist);
app.player.get_playlist().on('add',reload_playlist);
app.player.get_playlist().on('remove',reload_playlist);
app.player.get_playlist().on('sort',reload_playlist);

app.player.fetch_playlist(app.id);

function reload_playlist(videos){
  if ( app.player.get_next() ){
    $('#sacame-el-video').show();
  } else {
    $('#sacame-el-video').hide();
  }
  template = _.template($('#video-list-template').html());
  $('#table-container').html(template({videos : app.player.get_playlist()}));
  $('#table-container tbody').sortable({ itemSelector: 'tr',
    onDrop: function (item, container, _super){
      app.player.get_playlist().rebuild_indexes(item.data('cid'), item.index() + 1);
      _super(item, container);
    },
  });
}

function check_status(){
  if ( app.player.ended() && app.player.get_next() ){
    app.player.play_next();
  } else if ( app.player.ended() && app.player.get_current() && !app.player.get_next() ){
    app.player.get_current().set_playing(false);
  }
}

function onYouTubePlayerAPIReady() {
  app.api_loaded = true;
  app.trigger_api_loaded();
}




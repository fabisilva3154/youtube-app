
app = {};
app.models      || ( app.models      = {} );
app.collections || ( app.collections = {} );
app.api_loaded_callbacks = [];
app.add_api_loaded_callback = function(callback){
	app.api_loaded_callbacks.push(callback);
};
app.trigger_api_loaded = function(){
	_.each(app.api_loaded_callbacks, function(callback){
		callback();
	});
};





app.collections.Playlists = Backbone.Collection.extend({
	model : app.models.Playlist,
  url   : 'php_ajax/get_playlists.php',
});

app.collections.Videos = Backbone.Collection.extend({
	model : app.models.Video,

  comparator : function(video_a, video_b){
    console.log('afafss')
    return video_a.get('num_order') > video_b.get('num_order');
  },

  get_next_video_to_play : function(){
    return this.findWhere({ played : false });
  },

  get_current_video : function(){
    return this.findWhere({ is_playing : true });
  },

  get_video_by_id : function(video_id){
    return this.findWhere({ video_id : video_id });
  },

  rebuild_indexes : function(video_cid, new_index){
    var $this        = this;
    var sorted_video = this.get(video_cid);
    var old_index    = sorted_video.get('num_order');
    this.each(function(video){
      if ( video.get('num_order') <= new_index ){ 
        if ( video.get('num_order') > old_index ){
          video.set({ num_order : video.get('num_order') - 1 });
        }
        return ;
      }
      if ( video.cid == sorted_video.cid ){ return ; }      
      video.set({ num_order : video.get('num_order') + 1 });
    });
    sorted_video.set({ num_order : new_index });
    this.sort()
    this.each(function(video){
      console.log(video.get('num_order'), video.get('title'))
    });
  },

});
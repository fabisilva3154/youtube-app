// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// A generic onclick callback function.
function add_video(info, tab) {
  var method = 'POST';
  var url = 'http://www.yotuve.com.ar/php_ajax/add_video.php';
  var video_url = info.linkUrl;
  var video_id  = get_video_id_by_url(video_url);
  var playlist_id = '55ad6f01d3971';
  var xhr = new XMLHttpRequest();
  var formData = new FormData();
  formData.append('video_id', video_id);
  formData.append('id', playlist_id);
  formData.append('numOrder', 9);
  get_video_info(function(response){
      console.log(response);
      formData.append('thumbnail', response.items[0].snippet.thumbnails.default.url);
      formData.append('title', response.items[0].title);
      xhr.open(method, url, true);
      xhr.send(formData);
      xhr.onreadystatechange = function(){ 
          if(xhr.readyState == 4 && xhr.status == 200){ 
            alert("Agregado");
          }
      }
  }, video_id);
}

function get_video_id_by_url(url){
  if (url.indexOf('feature') != -1){
      start_slice = url.indexOf('&v=')+3;
      end_slice = url.substr(url.indexOf('&')+1).indexOf('&') != -1 ? url.indexOf('&') :  url.length;
  } else {
      start_slice = url.indexOf('?v=')+3;
      end_slice = url.indexOf('&') != -1 ? url.indexOf('&') : url.length;
  }
  video_id  = url.slice(start_slice, end_slice);
  return video_id;
}

function get_video_info(callback, video_id){
  var url = 'https://www.googleapis.com/youtube/v3/videos?id='+video_id+'&key=AIzaSyAldqwLI-PJ1uLXowjR7rvnCzQSii_9RLc&part=snippet,contentDetails,statistics,status';
  var method = 'GET';
  var xhr = new XMLHttpRequest();
  xhr.open(method, url, true);
  xhr.send();
  xhr.onreadystatechange = function(){ 
      if(xhr.readyState == 4 && xhr.status == 200){ 
        callback(JSON.parse(xhr.response));
      }
  }

}

// Create one test item for each context type.
var contexts = ["page","selection","link","editable","image","video",
                "audio"];
for (var i = 0; i < contexts.length; i++) {
  var context = contexts[i];
  var title = "Add video to yotuve.com.ar";
  var id = chrome.contextMenus.create({"title": title, "contexts":[context],
                                       "onclick": add_video});
  console.log("'" + context + "' item:" + id);
}

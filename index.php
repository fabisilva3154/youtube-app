<?php

  if ( !isset($_GET['id']) ){
    $id = uniqid();
    header('location:/yotuve/?id='.$id);
  } else {
    session_start();
    require('vendor/autoload.php');
    require('config/bootstrap.php');

    $id = $_GET['id'];
    $_SESSION['id'] = $id;
    $playlistRepo = $entityManager->getRepository('Playlist');
    $playlist     = $playlistRepo->find($_GET['id']);
    $name = $playlist ? $playlist->getName() : 'Sin título';
    include('yotuve.html');
  }
?>

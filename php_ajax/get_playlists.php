<?php

  require('../vendor/autoload.php');
  require('../config/bootstrap.php');

  $playlistRepository = $entityManager->getRepository('Playlist');
  $playlists = $playlistRepository->findAll();

  $playlistsJson = array();
  foreach ($playlists as $playlist) {
      array_push($playlistsJson, array(
          'id'   => $playlist->getId(),
          'name' => $playlist->getName(),
      ));
  }
  echo json_encode($playlistsJson);
?>
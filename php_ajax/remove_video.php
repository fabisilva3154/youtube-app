<?php
  
  require('../vendor/autoload.php');  
  require('../config/bootstrap.php');  
  if ( $_POST['id'] )
  {

    $playlist = $entityManager->find('Playlist', $_POST['id']);
    $videoPlaylistRepository = $entityManager->getRepository('VideoInPlaylist');
    // $video = $videoPlaylistRepository->findOneBy(array('video' => $_POST['video_id'], 'numOrder' => (int)$_POST['numOrder']));
    $video = $videoPlaylistRepository->findOneBy(array('video' => $_POST['video_id']));
    $playlist->removeVideo($video);
    $entityManager->persist($playlist);
    $entityManager->persist($video);

    $entityManager->flush();
    echo json_encode(array('response' => 'success'));
  }
?>
<?php
  
  session_start();

  require('../vendor/autoload.php');
  require('../config/bootstrap.php');

  if ( isset($_GET['name']) ){
    
    $playlistRepo = $entityManager->getRepository('Playlist');
    $playlist     = $playlistRepo->find($_SESSION['id']);
    $playlist->setName($_GET['name']);
    $entityManager->persist($playlist);
    $entityManager->flush();

  }
?>
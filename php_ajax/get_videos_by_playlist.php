<?php

  require('../vendor/autoload.php');
  require('../config/bootstrap.php');

  if ( isset($_GET['id']) ){
    $videoInPlaylistRepo = $entityManager->getRepository('VideoInPlaylist');
    $videos = $videoInPlaylistRepo->findBy(array('playlist'=> $_GET['id']));

    $videosJson = array();
    foreach ($videos as $video) {
        array_push($videosJson, array(
            'video_id'    => $video->getVideo()->getId(),
            'title' => $video->getVideo()->getTitle(),
            'thumbnail' => $video->getVideo()->getThumbnail(),
            'num_order' => $video->getNumOrder(),
            'saved' => true,
        ));
    }
    echo json_encode($videosJson);
  }
?>
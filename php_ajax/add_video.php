<?php
  
  require('../vendor/autoload.php');  
  require('../config/bootstrap.php');  
  if ( $_POST['id'] )
  {

    $playlist = $entityManager->find('Playlist', $_POST['id']);
    if ( !$playlist )
    {
        $playlist = new Playlist();
        $playlist->setId($_POST['id']);
        $playlist->setName('Sin título');
        $entityManager->persist($playlist);
    }

    $video = $entityManager->find('Video', $_POST['video_id']);
    if ( !$video )
    {
        $video = new Video();
        $video->setId($_POST['video_id']);
        $video->setTitle($_POST['title']);
        $video->setThumbnail($_POST['thumbnail']);
        $entityManager->persist($video);
    }

    $videoInPlaylist = new VideoInPlaylist();
    $videoInPlaylist->setVideo($video);
    $videoInPlaylist->setNumOrder($_POST['numOrder']);
    $playlist->addVideo($videoInPlaylist);
    $entityManager->persist($videoInPlaylist);

    $entityManager->flush();
    echo json_encode(array('response' => 'success'));
  }
?>